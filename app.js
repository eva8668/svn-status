var express = require('express');
var engine = require('ejs-locals');
var flash = require('connect-flash');
var router = require('./routes/index');
var http = require('http');
var path = require('path');
var settings = require('./settings');
var notifyHandler = require('./notifyHandler');

var app = module.exports = express();

// use ejs-locals for all ejs templates
app.engine('ejs', engine);

// all environments
app.set('port', process.env.PORT || settings.port);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser());
app.use(flash());
app.use(express.session({
	secret: settings.cookieSecret
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(function(req, res, next) {
  var locals = res.locals;
  locals.error = req.flash('error');
  locals.success = req.flash('success');
  next();
});
app.use(app.router);

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// Set up router
router.route(app);

// Start notification handler
var server = http.createServer(app);
notifyHandler.start(server);

// Start server
server.listen(app.get('port'), function(){
  console.log('Express server listening on port %d in %s mode', app.get('port'), app.get('env'));
});
