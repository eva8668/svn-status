var moment = require('moment'),
    dbHandler = require("../dbHandler"),
    notifyHandler = require('../notifyHandler');

exports.route = function(app) {
  app.get('/dashboard', function(req, res) {
    dbHandler.open(function(db) {
      var sql = 'SELECT * FROM repository ORDER BY name';
      db.all(sql, function(err, rows) {
        if (!err) {
          res.render('index', {
            title: 'SVN Status Dashboard',
            repos: formatRepo(rows)
          });
        } else {
          sendError(res, err);
        }
      });
    });
  });
  
  app.get('/course', function(req, res) {
    res.render('course', {
      title: '[學程]嵌入式 Linux 開發實務'
    });
  });

  app.get('/commit', function(req, res) {
    // Check parameters
    var repo = req.query['repo'];
    var revision = req.query['rev'];
    var time = req.query['time'];

    if (repo != null || revision != null) {
      // Save data to database
      dbHandler.open(function(db) {
        var sql = 'SELECT * FROM repository WHERE name = ?';
        db.get(sql, repo, function(err, row) {
          if (row && row.revision >= revision) {
            // Prevent revision from rolling back;
            return res.end();
          }
          
          var current = (time != null) ? new Date(time) : new Date();
          sql = (row) ? 'UPDATE repository SET revision = ?, lastrevisiontime = ? WHERE name = ?'
                      : 'INSERT INTO repository (name, revision, lastrevisiontime) VALUES (?, ?, ?)';
          var params = (row) ? [revision, current, repo]
                             : [repo, revision, current];

          db.run(sql, params, function(err) {
            if (!err) {
              // Notify all clients via Socket.io
              notifyHandler.updateRevision({
                repo: repo,
                revision: revision,
                time: moment(current).format('YYYY/MM/D HH:mm:ss')
              });
            } else {
              console.log(err);
            }
            res.end();
          });
        });
      });

    } else {
      res.end();
    }
  });

  app.get('/backup', function(req, res) {
    // Check parameters
    var repo = req.query['repo'];
    
    if (repo != null) {
      dbHandler.open(function(db) {
        var sql = 'SELECT * FROM repository WHERE name = ?';
        db.get(sql, repo, function(err, row) {
          var current = new Date();
          sql = (row) ? 'UPDATE repository SET lastbackuptime = ? WHERE name = ?'
                      : 'INSERT INTO repository (lastbackuptime, name) VALUES (?, ?)';
          var params = (row) ? [current, repo]
                             : [current, repo];

          db.run(sql, params, function(err) {
            if (!err) {
              // Notify all clients via Socket.io
              notifyHandler.updateBackupTime({
                repo: repo,
                backuptime: moment(current).format('YYYY/MM/D HH:mm:ss')
              });
            } else {
              console.log(err);
            }
            res.end();
          });
        });
      });

    } else {
      res.end();
    }
  });

  /*
  app.get('/u/:user', function(req, res) {
    User.get(req.params.user, function(err, user) {
      if (!user) {
        req.flash('error', '使用者不存在');
        return res.redirect('/');
      }
      
      Post.get(user.name, function(err, posts) {
        if (err) {
          req.flash('error', err);
          return res.redirect('/');
        }
        
        res.render('user', {
          title: user.name,
          posts: formatPost(posts)
        });
      });
    });
  });
  */

  function sendResponse(response, result) {
    response.writeHead(200, {
      "Content-type": "text/plain"
    });
    if (result) {
      response.write((typeof result === 'string') ? result : JSON.stringify(result));
    }
    response.end();
  }

  function sendError(response, error) {
    console.error(error);
    sendResponse(response, {
      error: error
    });
  }

  function formatRepo(repos) {
    repos.forEach(function(repo, index) {
      if (!repo.revision || !repo.lastrevisiontime) {
        repo.revision = '-';
        repo.lastRevTimeString = '-';
      } else {
        //repo.lastRevTimeString = moment(repo.lastrevisiontime).fromNow();
        repo.lastRevTimeString = moment(repo.lastrevisiontime).format('YYYY/MM/D HH:mm:ss');
      }
      
      if (!repo.lastbackuptime) {
        repo.lastBackupTimeString = '-';
      } else {
        //repo.lastBackupTimeString = moment(repo.lastbackuptime).fromNow();
        repo.lastBackupTimeString = moment(repo.lastbackuptime).format('YYYY/MM/D HH:mm:ss');
      }
      
    });
    return repos;
  }

};
