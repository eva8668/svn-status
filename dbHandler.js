var sqlite3 = require('sqlite3').verbose();
var dbFile = "./db/db.sqlite";

// XXX: For Demo -> Create table
var db = new sqlite3.Database(dbFile /* ':memory:' */); // create database in memory (not persistent!!)
db.close();

//XXX: API Documentation -> https://github.com/mapbox/node-sqlite3/wiki/API
function open(callback) {
	if (callback) {
		var db = new sqlite3.Database(dbFile);
		callback(db);
		
		// Remember to database every time we finish querying
		// The close() function will wait until all pending queries are completed before closing the database.
		db.close();
	}
}

exports.open = open;