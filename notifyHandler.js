var io;

// XXX: API Documentation --> https://github.com/LearnBoost/Socket.IO/wiki/Configuring-Socket.IO
function start(httpServer) {
	io = require('socket.io')(httpServer);
	
	/*
	io = socketIO.listen(httpServer);
	io.enable('browser client minification');  // send minified client
	io.enable('browser client etag');          // apply etag caching logic based on version number
//	io.enable('browser client gzip');          // gzip the file
	io.set('log level', 1);                    // reduce logging
	*/
	
	io.on('connection', function(socket) {
		console.log("Socket[" + socket.id + "] has connected!");
		// Handle socket incoming events
		//socket.on('control event', function(data) {
			// sending to all clients except sender
			//socket.broadcast.emit('control event', data);
		//});

		// Handle socket disconnection
		socket.on('disconnect', function() {
			console.log("Socket[" + socket.id + "] has disconnected!");
		});
		
//		// send to current request socket client
//		socket.emit('message', "this is a test");
//
//		// sending to all clients, include sender
//		io.sockets.emit('message', "this is a test");
//
//		// sending to all clients except sender
//		socket.broadcast.emit('message', "this is a test");
//
//		// sending to all clients in 'game' room(channel) except sender
//		socket.broadcast.to('game').emit('message', 'nice game');
//
//		// sending to all clients in 'game' room(channel), include sender
//		io.sockets.in('game').emit('message', 'cool game');
//
//		// sending to individual socketid
//		io.sockets.socket(socketid).emit('message', 'for your eyes only');
	});
}

// sending to all clients
exports.updateRevision = function(data) {
	io.sockets.emit('new commit', data);
};

exports.updateBackupTime = function(data) {
	io.sockets.emit('new backup', data);
};

exports.start = start;